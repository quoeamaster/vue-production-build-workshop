import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import fs from 'fs'

// https://vitejs.dev/config/
export default defineConfig({
  // configure the port to 3000 instead of 5173 (default)
  server: {
    port: 3000,
    https: {
      key: fs.readFileSync('key-1.pem'),
      cert: fs.readFileSync('cert-1.pem'),
    },
  },
  plugins: [vue()],
})
